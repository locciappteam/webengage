﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebEngage.Data.ViewModels
{
    [Table("W_ENG_EVENT_CHARGED_DATA")]
    public class W_ENG_EVENT_CHARGED_DATA
    {
        public string ORDER_NUMBER { get; set; }
        public DateTime DATE { get; set; }
        public string CUSTOMER { get; set; }
        public string STORE { get; set; }
        public string STORE_NAME { get; set; }

        public string PRODUCTS { get; set; }
        public string PRODUCT_NAMES { get; set; }
        public string MARKDOWN { get; set; }
        public int? QTY { get; set; }
        public decimal? UNIT_PRICE { get; set; }
        public decimal? TOTAL_PRICE_LINE { get; set; }
        public decimal? TOTAL_AFTER_DISCOUNT { get; set; }
    }
}
