﻿using System;
using System.Collections.Generic;
using WebEngage.Data.Models;

namespace WebEngage.Data.Interfaces
{
    public interface IRepository<T>:IDisposable where T:DEFAULT_ENTITY
    {
        void Save();
        void Rollback();
        T Add(T entity);
        IEnumerable<T> AddList(IEnumerable<T> entity);
        T Edit(T entity);
        void Delete(T entity);
        IEnumerable<T> Get();
        IEnumerable<T> GetTopOf(int rows);
        T GetByID(object id);
    }
}
