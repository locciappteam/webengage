﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebEngage.Data.Models
{
    [Table("ENG_PARAMETERS")]
    public class ENG_PARAMETER : DEFAULT_ENTITY
    {
        public string PARAMETER { get; set; }

        public string DESCRIPTION { get; set; }

        public string VALUE { get; set; }

        public string TYPE { get; set; }

        public bool? ACTIVE { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CREATED { get; set; }

        public string ALTERED_BY { get; set; }

        public DateTime? ALTERED { get; set; }
    }
}
