﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebEngage.Data.Models
{
    [Table("ENG_CUSTOMERS_QUEUE")]
    public class ENG_CUSTOMER_QUEUE : DEFAULT_ENTITY
    {
        public string VIP_CODE { get; set; }

        public string STORECODE { get; set; }

        public string TITLE { get; set; }

        public string LAST_NAME { get; set; }

        public string FIRST_NAME { get; set; }

        public string ADDRESS1 { get; set; }

        public string ADDRESS2 { get; set; }

        public string ADDRESS3 { get; set; }

        public string CITY { get; set; }

        public string STATE { get; set; }

        public string POST_CODE { get; set; }

        public string COUNTRY { get; set; }

        public string PHONE { get; set; }

        public string MOBILE { get; set; }

        public string PHONE3 { get; set; }

        public string EMAIL { get; set; }

        public string DAY_BIRTH { get; set; }

        public string MONT_BIRTH { get; set; }

        public string YEAR_BIRTH { get; set; }

        public string SEX { get; set; }

        public string T_ETABLISSEMENT { get; set; }

        public string YTC_CODECPFCNPJ { get; set; }

        public string T_EMAILING { get; set; }

        public DateTime? T_CREATION { get; set; }
    }
}
