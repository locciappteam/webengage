﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebEngage.Data.Models
{
    [Table("ENG_ORDERS_QUEUE")]
    public class ENG_ORDER_QUEUE : DEFAULT_ENTITY
    {
        public string STORE { get; set; }

        public string CAIS { get; set; }

        public string ORDER_NUMBER { get; set; }

        public string DATE { get; set; }

        public string CUSTOMER { get; set; }

        public string CURRENCY { get; set; }

        public string ITEMNUMBER { get; set; }

        public int? QTDE { get; set; }

        public decimal? UNIT_PRICE { get; set; }

        public decimal? TOTAL_PRICE_LINE { get; set; }

        public decimal? TOTAL_PRICE_DISC_LINE { get; set; }

        public string MARKDOWN { get; set; }

        public string FOOTER_DISCOUNT { get; set; }

        public string TAXCODE { get; set; }

        public string DEPOT { get; set; }
    }
}
