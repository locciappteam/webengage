﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebEngage.Data.Models
{
    [Table("ENG_EVENT_LOG")]
    public class ENG_EVENT_LOG : DEFAULT_ENTITY
    {
        public long ID { get; set; }
        public DateTime CREATED { get; set; }
        public string EVENT { get; set; }
        public DateTime  ORDER_DATE { get; set; }
        public string ORDER_NUMBER { get; set; }
        public string ORDER_STORE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string STATUS { get; set; }
        public string ERROR_MESSAGE { get; set; }
    }
}
