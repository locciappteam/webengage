﻿using Microsoft.EntityFrameworkCore;
using WebEngage.Data.Models;
using WebEngage.Data.ViewModels;

namespace WebEngage.Data
{
    public class DataContext : DbContext
    {
        private string _connStr { get; set; }
        public string _typeEnvironment { get; set; }

        public DbSet<ENG_PARAMETER> engParameters { get; set; }
        public DbSet<ENG_ORDER_QUEUE> engOrderQueue { get; set; }
        public DbSet<ENG_CUSTOMER_QUEUE> engCustomersQueue { get; set; }
        public DbSet<ENG_EVENT_LOG> engEventLog { get; set; }
        // VIEW MODEL 
        public DbSet<W_ENG_EVENT_CHARGED_DATA> wEngEventChargedData { get; set; }
        public DataContext(int dbMode = 1)
        {
            switch (dbMode)
            {
                case 1:
                {
                    _connStr = @"Data Source=AMSAOLNXDB02;Initial Catalog=INTEGRACAO_FRANQUIA_NOVO;Persist Security Info=True;User ID=usrEngage;Password=3ngag3";
                    _typeEnvironment = "Production";
                }
                break;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connStr);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ENG_PARAMETER>().HasKey(p => p.PARAMETER);
            
            modelBuilder.Entity<ENG_EVENT_LOG>().HasKey(p => p.ID);
            modelBuilder.Entity<ENG_EVENT_LOG>().Property(p => p.ID).ValueGeneratedOnAdd();

            modelBuilder.Entity<ENG_CUSTOMER_QUEUE>().HasNoKey(); 
            modelBuilder.Entity<ENG_ORDER_QUEUE>().HasNoKey();
            modelBuilder.Entity<W_ENG_EVENT_CHARGED_DATA>().HasNoKey();
        }
    }
}
