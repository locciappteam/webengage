﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebEngage.Data.Json.Post
{
    public class User
    {
        public string userId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string birthDate { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string company { get; set; }

        public Attributes attributes { get; set; }
    }

    public class Attributes
    {
        public Attributes()
        {
            Age = "";
            Twitterusername = "";
            Dollarsspent = 0;
            Pointsearned = 0;
            storeID = "";
            Platform = "pos";
        }
        public string Age { get; set; }
        public string Twitterusername { get; set; }
        public double? Dollarsspent { get; set; }
        public int? Pointsearned { get; set; }

        [JsonProperty(PropertyName = "Store ID")]
        public string storeID { get; set; }

        public string CPF { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Platform { get; set; }
    
    }
}
