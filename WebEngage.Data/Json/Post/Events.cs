﻿using Newtonsoft.Json;
using System;

namespace WebEngage.Data.Json.Post
{
    public class EventCharged
    {
        public string userId { get; set; }
        public string eventName { get; set; }
        public string eventTime { get; set; }
        public EventChargedData eventData { get; set; }


        public EventCharged()
        {
            eventName = "Charged";
            eventTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + DateTime.Now.ToString("zzz").Replace(":","");
        }

    }
    public class EventChargedData
    {
        [JsonProperty(PropertyName = "Additional Discount")]
        public decimal? Additional_Discount { get; set; }

        [JsonProperty(PropertyName = "Coupon Code")]
        public string Coupon_Code { get; set; }

        [JsonProperty(PropertyName = "Delivery Date")]
        public string Delivery_Date { get; set; }

        [JsonProperty(PropertyName = "Discount Amount")]
        public decimal? Discount_Amount { get; set; }

        [JsonProperty(PropertyName = "Gift Card")]
        public int Gift_Card { get; set; }

        [JsonProperty(PropertyName = "Gross Order Amount")]
        public decimal? Gross_Order_Amount { get; set; }

        [JsonProperty(PropertyName = "Net Order Amount")]
        public decimal? Net_Order_Amount { get; set; }

        [JsonProperty(PropertyName = "Order Amount")]
        public decimal? Order_Amount { get; set; }

        [JsonProperty(PropertyName = "Order Date")]
        public string Order_Date { get; set; }

        [JsonProperty(PropertyName = "Payment Mode")]
        public string Payment_Mode { get; set; }

        [JsonProperty(PropertyName = "Platform")]
        public string Platform { get; set; }

        [JsonProperty(PropertyName = "Product Names")]
        public string Product_Names { get; set; }

        [JsonProperty(PropertyName = "Product SKU")]
        public string Product_SKU { get; set; }

        [JsonProperty(PropertyName = "Store Name")]
        public string Store_Name { get; set; }

        [JsonProperty(PropertyName = "Store ID")]
        public string Store_ID { get; set; }

        [JsonProperty(PropertyName = "Total Amount")]
        public decimal? Total_Amount { get; set; }

        [JsonProperty(PropertyName = "Cart Item Count")]
        public int? Cart_Item_Count { get; set; }

        [JsonProperty(PropertyName = "Order ID")]
        public string Order_ID { get; set; }

        [JsonProperty(PropertyName = "Shipping Charges")]
        public int? Shipping_Charges { get; set; }


    }

}
