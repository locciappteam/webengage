﻿using WebEngage.Data.Interfaces;
using WebEngage.Data.Models;

namespace WebEngage.Data.Repositories
{
    public class RPT_ENG_PARAMETER : Repository<ENG_PARAMETER>, IRPT_ENG_PARAMETER
    {
        public RPT_ENG_PARAMETER(): base(new DataContext())
        {

        }
    }
}
