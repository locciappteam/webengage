﻿using WebEngage.Data.Interfaces;
using WebEngage.Data.Models;

namespace WebEngage.Data.Repositories
{
    public class RPT_ENG_EVENT_LOG : Repository<ENG_EVENT_LOG>, IRPT_ENG_EVENT_LOG
    {
        public RPT_ENG_EVENT_LOG(): base(new DataContext())
        {

        }
    }
}
