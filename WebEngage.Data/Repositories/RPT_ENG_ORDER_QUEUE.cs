﻿using WebEngage.Data.Interfaces;
using WebEngage.Data.Models;

namespace WebEngage.Data.Repositories
{
    public class RPT_ENG_ORDER_QUEUE : Repository<ENG_ORDER_QUEUE>, IRPT_ENG_ORDER_QUEUE
    {
        public RPT_ENG_ORDER_QUEUE(): base(new DataContext())
        {

        }
    }
}
