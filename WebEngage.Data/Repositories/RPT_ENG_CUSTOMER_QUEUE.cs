﻿using WebEngage.Data.Interfaces;
using WebEngage.Data.Models;

namespace WebEngage.Data.Repositories
{
    public class RPT_ENG_CUSTOMER_QUEUE : Repository<ENG_CUSTOMER_QUEUE>, IRPT_ENG_CUSTOMER_QUEUE
    {
        public RPT_ENG_CUSTOMER_QUEUE(): base(new DataContext())
        {

        }
    }
}
