﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebEngage.Data;
using WebEngage.Data.Json.Post;
using WebEngage.Data.Models;
using WebEngage.Data.Repositories;
using WebEngage.Data.ViewModels;

namespace WebEngage.Components
{
    public class WebAPI
    {
        public string host { get; set; }
        public string licenseCode { get; set; }
        public string version { get; set; }
        public string apiKey { get; set; }
        public WebAPI(string dbMode = "staging")
        {
            dynamic environment = "";
            dynamic jsonParams = JsonConvert.DeserializeObject(new RPT_ENG_PARAMETER().GetByID("WEBAPI").VALUE);

            if (dbMode == "staging")
                environment = jsonParams.WebAPI.Staging;

            if (dbMode == "production")
                environment = jsonParams.WebAPI.Production;

            host = environment.host;
            licenseCode = environment.licenseCode;
            version = environment.version;
            apiKey = environment.apiKey;
        }
        public async Task GetCustomerData(string userId= null)
        {
            try
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                using (var client = new HttpClient(clientHandler))
                {
                    client.Timeout = TimeSpan.FromMinutes(5);
                    Uri uri = new Uri(host);

                    client.BaseAddress = uri;
                    client.DefaultRequestHeaders.Add("Authorization","Bearer " + apiKey);

                    string urlSend = $"/{version}/accounts/{licenseCode}/users";
                    if (userId != null)
                        urlSend += $"?userId={userId}";

                    using (var response = client.GetAsync(urlSend).Result)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            var retorno = responseData;
                        }

                    }

                }
            }
            catch(Exception)
            {

            }
        }
        public string GetSendOrderList()
        {
            try
            {
                using var _ctx = new DataContext();

                List<W_ENG_EVENT_CHARGED_DATA> listOrders = _ctx.Set<W_ENG_EVENT_CHARGED_DATA>().ToList();
                List<ENG_CUSTOMER_QUEUE> listCustomer = new RPT_ENG_CUSTOMER_QUEUE().Get().ToList();

                foreach (var customer in listCustomer)
                {
                    User newUser = new User();
                    newUser.userId = customer.YTC_CODECPFCNPJ;
                    newUser.firstName = customer.FIRST_NAME;
                    newUser.lastName = customer.LAST_NAME;
                    newUser.gender = customer.SEX.Trim().ToUpper().Contains("F") ? "FEMALE" : "MALE";
                    newUser.phone = customer.PHONE;
                    newUser.email = customer.EMAIL;
                    
                    if ( (customer.DAY_BIRTH != "" && customer.DAY_BIRTH != "0") && (customer.MONT_BIRTH != "" && customer.MONT_BIRTH != "0") && (customer.YEAR_BIRTH != "" && customer.YEAR_BIRTH != "0"))
                    {
                        newUser.birthDate = new DateTime(Convert.ToInt32(customer.YEAR_BIRTH), Convert.ToInt32(customer.MONT_BIRTH), Convert.ToInt32(customer.DAY_BIRTH)).ToString("yyyy-MM-ddTHH:mm:ss") + DateTime.Now.ToString("zzz").Replace(":", "");
                    }

                    newUser.attributes = new Attributes
                    {
                        storeID = customer.STORECODE,
                        CPF = customer.YTC_CODECPFCNPJ,
                        State = customer.STATE,
                        City = customer.CITY
                    };

                    string jsonStr = JsonConvert.SerializeObject(newUser);
                    var custResult = SendCustomerData(jsonStr).Result;

                    var lstorder = listOrders.Where(p=> p.CUSTOMER == customer.YTC_CODECPFCNPJ).ToList();

                    if (lstorder != null)
                    {
                        foreach (var order in lstorder)
                        {
                            EventCharged newEvent = new EventCharged();
                            newEvent.userId = order.CUSTOMER;
                            newEvent.eventData = new EventChargedData();

                            var auxData = order.DATE.ToString("yyyy-MM-ddTHH:mm:ss") + DateTime.Now.ToString("zzz").Replace(":", "");

                            newEvent.eventData.Coupon_Code = order.MARKDOWN;
                            newEvent.eventData.Order_Date = auxData;
                            newEvent.eventData.Store_ID = order.STORE;
                            newEvent.eventData.Store_Name = order.STORE_NAME;
                            newEvent.eventData.Product_SKU = order.PRODUCTS;
                            newEvent.eventData.Product_Names = order.PRODUCT_NAMES;
                            newEvent.eventData.Platform = "pos";
                            newEvent.eventData.Order_ID = order.ORDER_NUMBER;
                            newEvent.eventData.Coupon_Code = order.MARKDOWN;
                            newEvent.eventData.Gross_Order_Amount = order.TOTAL_PRICE_LINE;
                            newEvent.eventData.Net_Order_Amount = order.TOTAL_PRICE_LINE;
                            newEvent.eventData.Order_Amount = order.TOTAL_AFTER_DISCOUNT;
                            newEvent.eventData.Discount_Amount = (order.TOTAL_PRICE_LINE - order.TOTAL_AFTER_DISCOUNT);
                            //newEvent.eventData.Total_Amount = order.TOTAL_AFTER_DISCOUNT;
                            newEvent.eventData.Shipping_Charges = 0;

                            jsonStr = JsonConvert.SerializeObject(newEvent);
                            var resultOrder = SendEventData(jsonStr).Result;

                            ENG_EVENT_LOG log = new ENG_EVENT_LOG();
                            log.CREATED = DateTime.Now;
                            log.EVENT = "CHARGED";
                            log.CUSTOMER_ID = customer.YTC_CODECPFCNPJ;
                            log.ORDER_NUMBER = order.ORDER_NUMBER;
                            log.ORDER_DATE = order.DATE;
                            log.ORDER_STORE = order.STORE;


                            if (resultOrder.IsSuccessStatusCode && custResult.IsSuccessStatusCode)
                            {
                                log.STATUS = "SUCCESS";
                            }
                            else
                            {
                                log.STATUS = "ERROR";
                                if (!resultOrder.IsSuccessStatusCode)
                                    log.ERROR_MESSAGE = resultOrder.StatusCode.ToString();

                                if (!custResult.IsSuccessStatusCode)
                                    log.ERROR_MESSAGE = custResult.StatusCode.ToString();
                            }

                            using (RPT_ENG_EVENT_LOG _rpt = new RPT_ENG_EVENT_LOG())
                            {
                                _rpt.Add(log);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        public async Task<HttpResponseMessage> SendCustomerData(string userJson)
        {
            try
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                using (var client = new HttpClient(clientHandler))
                {
                    client.Timeout = TimeSpan.FromMinutes(5);
                    Uri uri = new Uri(host);

                    client.BaseAddress = uri;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + apiKey);

                    string urlSend = $"/{version}/accounts/{licenseCode}/users";

                    //string jsonStr = JsonConvert.SerializeObject(userJson);

                    var content = new StringContent(userJson, Encoding.UTF8, "application/json");

                    using (var response = client.PostAsync(urlSend, content).Result)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        return response;
                    }
                }
            }

            catch (Exception)
            {
                return null;
            }
        }
        public async Task<HttpResponseMessage> SendEventData(string jsonStr)
        {
            try
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                using (var client = new HttpClient(clientHandler))
                {
                    client.Timeout = TimeSpan.FromMinutes(5);
                    Uri uri = new Uri(host);

                    client.BaseAddress = uri;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + apiKey);

                    string urlSend = $"/{version}/accounts/{licenseCode}/events";

                    var content = new StringContent(jsonStr, Encoding.UTF8, "application/json");

                    using (var response = client.PostAsync(urlSend, content).Result)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        return response;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
