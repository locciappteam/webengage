using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebEngage.Components;
using WebEngage.Data;

namespace WebEngage.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IHostApplicationLifetime _hostApplicationLifetime;
        private IConfiguration _configuration;
        private int _runIntervalInDays;
        private string _webEngageEnvironment;
        private bool _executeProcedure;
        public Worker(ILogger<Worker> logger, IServiceScopeFactory serviceScopeFactory, IHostApplicationLifetime hostApplicationLifetime)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
            _hostApplicationLifetime = hostApplicationLifetime;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                    // EXECUTA A PROCEDURE1

                    if (_executeProcedure)
                    {
                        using (DataContext _ctx = new DataContext())
                        {
                            _ctx.Database.SetCommandTimeout(TimeSpan.FromMinutes(15));
                            SqlParameter prmIntervalDays = new SqlParameter
                            {
                                ParameterName = "PERIODO",
                                SqlDbType = System.Data.SqlDbType.Int,
                                Value = _runIntervalInDays
                            };

                            var result = _ctx.Database.ExecuteSqlRaw("EXEC usp_ENG_EXPORT_DAILY_ORDERS @PERIODO", prmIntervalDays);
                        }
                    }
                    // EXECUTA O PROCESSO
                    var getSendOrderList = new WebAPI(_webEngageEnvironment).GetSendOrderList();
                    await Task.Delay(TimeSpan.FromDays(_runIntervalInDays));
                }

            }
            catch when (stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation($"Sender is stopping");
            }
            catch (Exception ex) when (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogError(ex, "Sender had error");
            }
            finally
            {
                _hostApplicationLifetime.StopApplication();
            }
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _configuration = _serviceScopeFactory.CreateScope().
                          ServiceProvider.GetRequiredService<IConfiguration>();

            _runIntervalInDays  = int.Parse(_configuration["App.Configurations:RunIntervalInDays"]);
            _webEngageEnvironment = _configuration["App.Configurations:WebEngageEnvironment"].ToLower();
            _executeProcedure = bool.Parse(_configuration["App.Configurations:ExecProcEngExportDailyOrders"]);
            return base.StartAsync(cancellationToken);
        }

    }
}
